<?php
$opts = [
	"http" => [
		"method" => "GET",
		"header" => "x-api-key: AQBzmlGGE6PtIZbu5OalgsImUZBEL1Zpl_KtEtu\r\n"
	]
];

$CHANNELS = json_decode(file_get_contents("http://localhost:10080/1/channellist", false, stream_context_create($opts)), true);
$USERS = json_decode(file_get_contents("http://localhost:10080/1/clientlist?-away&-voice", false, stream_context_create($opts)), true);
$SERVERNAME = "TeamSpeak Bretzel";
$users = [];

foreach ($USERS["body"] as $u) {

	if (!$u["client_type"]) {
		if ($u["client_flag_talking"]) {
			$status = "player_on.svg";
		} elseif ($u["client_away"]) {
			$status = "away.svg";
		} elseif (!$u["client_output_hardware"]) {
			$status = "hardware_output_muted.svg";
		} elseif (!$u["client_input_hardware"]) {
			$status = "hardware_input_muted.svg";
		} elseif ($u["client_output_muted"]) {
			$status = "output_muted.svg";
		} elseif ($u["client_input_muted"]) {
			$status = "input_muted.svg";
		} else {
			$status = "player_off.svg";
		}
		
		$user = [
			"client_nickname" => $u["client_nickname"],
			"status" => $status
		 ];

		$users[$u["cid"]][] = $user;
	}
}

echo $SERVERNAME;
foreach ($CHANNELS["body"] as $channel) {
	if (substr($channel["channel_name"], 0, 8) == "[*spacer") {
		echo "<hr>\n";
	} elseif (substr($channel["channel_name"], 0, 8) == "[cspacer") {
		echo "<div id=\"center\">" . substr($channel["channel_name"], strrpos($channel["channel_name"], "]") + 1) . "</div>\n";
	} else {
		echo "<div id=\"channel\">" . $channel["channel_name"] . "</div>\n";
	}
	if (array_key_exists($channel["cid"], $users)) {
		echo "<ul>\n";
		foreach ($users[$channel["cid"]] as $u) {
			echo "<li id=\"user\">" . "<img src=\"resources/" . $u["status"] . "\"> " . $u["client_nickname"] . "</li>\n";
		}
		echo "</ul>\n";
	}
}